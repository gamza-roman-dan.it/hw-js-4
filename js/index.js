"use strict"

//1-
//Цикл – це конструкція в програмуванні, яка дає змогу виконувати дії, що повторюються,
// певну кількість разів або доки не виконається певна умова.

//2-
//є три види циклів у js: "for", "while", "do while"

//3-
//у "while" умова йде по переду, у "do while" навпаки, умова йде після тіла циклу.

//1-
let userNumberOne;
let userNumberTwo;

do {
    userNumberOne = +prompt("введіть перше число до п'яти");
} while (isNaN(userNumberOne) || userNumberOne < 0 || userNumberOne >5);

do {
    userNumberTwo = +prompt("введіть друге число від п'яти до двадцяти");
} while (isNaN(userNumberTwo) || userNumberTwo <= 5 || userNumberTwo >20);

for (let i =userNumberOne; i <= userNumberTwo; i++) {
    console.log(i);
}

//2-
let userNumberThree;

do {
    userNumberThree = prompt("введіть парне число");
} while (isNaN(userNumberThree) || userNumberThree % 2 !== 0);

alert("вітаю ви ввели правильне число!");